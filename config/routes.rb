Rails.application.routes.draw do
    devise_for :admins, only:[:sessions]
    devise_for :users, only:[:sessions]
    devise_scope :admin do
        authenticated :admin do
            root 'borrows#mypadentries', as: 'admin_root'
            resources :borrows, only: :index
            resources :borrows do
                collection do
                    get 'chargerentries', to: 'borrows#chargerentries', as: 'chargerentries'
                    get 'mypadentries', to: 'borrows#mypadentries', as: 'mypadentries'
                    get 'purge', to: 'borrows#purge', as: 'purge'
                end
             end
        end
    end

    devise_scope :user do
        authenticated :user do
            resources :borrows, only: :index
            root 'borrows#index', as: 'user_root'
            resources :borrows do
                collection do
                    get 'chargerborrows', to: 'borrows#chargerborrows', as: 'chargerborrows'
                end
            end
        end
    end

resources :borrows, only: :index
    resources :borrows do
        collection do
            get ':id/returnpad', to: 'borrows#returnpad', as:'returnpad'
            get ':id/returncharger', to: 'borrows#returncharger', as: 'returncharger'
        end
     end
    
resources :my_pads do
    collection do
        get 'chargepage', to: 'my_pads#chargepage', as: 'chargepage'
        get 'updatepage', to: 'my_pads#updatepage', as: 'updatepage'
        get 'repairpage', to: 'my_pads#repairpage', as: 'repairpage'
        get 'out', to: 'my_pads#out', as: 'out' 
        get ':id/charge', to: 'my_pads#charge', as: 'charge'
        get ':id/update', to: 'my_pads#update', as: 'update'
        get ':id/repair', to: 'my_pads#repair', as: 'repair'
    end
end
    
resources :chargers do
    collection do
        get 'out', to: 'chargers#out', as: 'out'
    end
end

root 'devise/sessions#new'
#root 'publics#home'
end
