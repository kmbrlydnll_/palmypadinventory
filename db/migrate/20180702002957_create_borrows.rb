class CreateBorrows < ActiveRecord::Migration
  def change
    create_table :borrows do |t|
      t.string :name
      t.string :position
      t.integer :id_number
      t.datetime :date_borrowed
      t.datetime :date_returned
      t.datetime :my_pad_expected_return
      t.datetime :charger_expected_return
      t.datetime :my_pad_actual_return
      t.datetime :charger_actual_return
      t.string :released_by
      t.string :flight_number

      t.timestamps null: false
    end
  end
end
