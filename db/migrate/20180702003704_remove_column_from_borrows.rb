class RemoveColumnFromBorrows < ActiveRecord::Migration
   def change
    remove_column :borrows, :date_returned, :datetime
  end
end
