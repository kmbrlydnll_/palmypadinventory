class CreateMyPads < ActiveRecord::Migration
  def change
    create_table :my_pads do |t|
      t.string :reference_number
      t.datetime :last_updated
      t.boolean :for_repair
      t.boolean :charging
      t.boolean :for_updating
      t.boolean :status

      t.timestamps null: false
    end
  end
end
