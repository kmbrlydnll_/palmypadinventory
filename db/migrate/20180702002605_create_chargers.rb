class CreateChargers < ActiveRecord::Migration
  def change
    create_table :chargers do |t|
      t.string :reference_number
      t.boolean :status

      t.timestamps null: false
    end
  end
end
