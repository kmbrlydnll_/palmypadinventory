class AddReferencesToBorrows < ActiveRecord::Migration
  def change
    add_reference :borrows, :my_pad, index: true, foreign_key: true
    add_reference :borrows, :charger, index: true, foreign_key: true
  end
end
