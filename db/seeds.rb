# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

if Admin.all.empty?
    Admin.create!([{email: 'admin@palmypadinventory.com', password: 'a34f500oo', password_confirmation: "a34f500oo"}])
end

if User.all.empty?
    User.create!([{email: 'user@palmypadinventory.com', password: '88re3321', password_confirmation: "88re3321"}])
end