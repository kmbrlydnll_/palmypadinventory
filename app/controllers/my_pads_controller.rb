class MyPadsController < ApplicationController
  def index
    @my_pad = MyPad.all.order('reference_number ASC') 
  end

  def new
    @my_pad = MyPad.new
  end

  def create
    @my_pad = MyPad.new(mypad_params)
      if @my_pad.save
        redirect_to my_pads_path
      else
        render 'my_pads/new'
      end
  end
  
  def edit
     @my_pad = MyPad.find(params[:id])
  end

  def destroy
     @my_pad = MyPad.find(params[:id])
     @my_pad.delete
     redirect_to my_pads_path
  end
  
  def charge
   @my_pad = MyPad.find(params[:id])
    if @my_pad.charging == false
      @my_pad.charging = true
      redirect_to my_pads_path
    else
      @my_pad.charging = false
      redirect_to chargepage_my_pads_path
    end
    @my_pad.save
    
  end
  
  def update
   @my_pad = MyPad.find(params[:id])
    if @my_pad.for_updating == false
      @my_pad.for_updating = true
      redirect_to my_pads_path
    else
      @my_pad.for_updating = false
      @my_pad.last_updated = DateTime.today
      redirect_to updatepage_my_pads_path
    end
    @my_pad.save
    
  end
  
  def repair
   @my_pad = MyPad.find(params[:id])
      if @my_pad.for_repair == false
        @my_pad.for_repair = true
        redirect_to my_pads_path
      else
        @my_pad.for_repair = false
        redirect_to repairpage_my_pads_path
      end
      @my_pad.save
  end 
   
  private
    def mypad_params
      params.require(:my_pad).permit(:reference_number, :last_updated, :status, :charging, :for_repair, :for_updating)
    end
end
