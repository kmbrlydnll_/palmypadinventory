class BorrowsController < ApplicationController
    def index
        @borrow = Borrow.all.order('name ASC') 
        respond_to do |format|
          format.html
          format.csv { send_data @borrow.to_csv, filename: "borrows-#{Date.today}.csv" }
        end
    end

  def new
    @borrow = Borrow.new
  end

  def create
    @borrow = Borrow.new(borrow_params)
    if @borrow.save
        
        #mypad
        @borrow.my_pad.status = false
        @borrow.my_pad.save
        
        #charger
        if @borrow.charger.to_s != "No Charger"
          @borrow.charger.status = false
          @borrow.charger.save
        else
          @borrow.charger.save
        end
      redirect_to borrows_path
    else
      render 'borrows/new'
    end
  end

  def edit
     @borrow = Borrow.find(params[:id])
  end

  def destroy
     @borrow = Borrow.find(params[:id])
     @borrow.delete
     redirect_to mypadentries_borrows_path
  end
  
  def returnpad
    @borrow = Borrow.find(params[:id])
    @borrow.my_pad_actual_return = DateTime.now
    @my_pad = MyPad.all
      @my_pad.each do |a|
        if @borrow.my_pad == a
             a.status = true
             a.save
             redirect_to borrows_path
        end
      end
    @borrow.save
  end
  
  def purge
    @borrow = Borrow.all
    @borrow.each do |a|
      a.destroy
    end
    redirect_to mypadentries_borrows_path
  end
  
  def returncharger
    @borrow = Borrow.find(params[:id])
    @borrow.charger_actual_return = DateTime.now
    @charger = Charger.all
      @charger.each do |a|
        if @borrow.charger == a
             a.status = true
             a.save
             redirect_to chargerborrows_borrows_path
        end
      end
    @borrow.save
  end
  
  private
    def borrow_params
      params.require(:borrow).permit!
    end
end
