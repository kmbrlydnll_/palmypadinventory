class ChargersController < ApplicationController
  def index
    @charger = Charger.all.order('reference_number ASC') 
  end

  def new
    @charger = Charger.new
  end
  
  def create
    @charger = Charger.new(charger_params)
      if @charger.save
        redirect_to chargers_path
      else
        render 'chargers/new'
      end
  end
  
  def edit
    @charger = Charger.find(params[:id])
  end

  def destroy
    @charger = Charger.find(params[:id])
    @charger.delete
    redirect_to chargers_path
  end
  
  private
    def charger_params
      params.require(:charger).permit(:reference_number, :status)
    end
end
