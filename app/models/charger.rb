class Charger < ActiveRecord::Base
    has_many :borrows
    validates :reference_number, presence: {message: "must be answered."}
    validates_associated :borrows
    
    def to_s
        self.reference_number
    end 
end
