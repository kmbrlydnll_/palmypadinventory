class Borrow < ActiveRecord::Base
    belongs_to :my_pad
    belongs_to :charger
    require 'csv'
    
    validates :name, :flight_number, :id_number, :position, :my_pad_id, :charger_id, :released_by, :charger_expected_return, :my_pad_expected_return, presence: {message: "must be answered" }
   # validates_associated :my_pad, :charger
    
    def self.to_csv(options ={})
        desired_columns = ["id", "name", "position", "flight_number", "date_borrowed", "my_pad_id", "my_pad_actual_return", "charger_id", "charger_actual_return", "released_by"]
        CSV.generate(options) do |csv|
            names = desired_columns << 'my_pad_elapsed_time'
            csv << names
            
            all.each do |borrow|
                if borrow.my_pad_actual_return == nil
                    row = borrow.attributes.values_at(*desired_columns) 
                else
                    elapsed_time = ((borrow.my_pad_actual_return - borrow.date_borrowed)/60).to_f
                    row = borrow.attributes.values_at(*desired_columns) << elapsed_time
                end
                csv << row
            end
        end
    end
end
