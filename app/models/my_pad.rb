class MyPad < ActiveRecord::Base
    has_many :borrows
    validates :reference_number, :last_updated, presence: {message: "must be answered."}
    validates_associated :borrows
    
    def to_s
        self.reference_number
    end 
end
